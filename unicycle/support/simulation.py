from typing import Tuple

import numpy as np
import pygame

from acados_template import AcadosOcpSolver, AcadosSimSolver
from unicycle.support.utils import plot_robot


class RobotSimulation:
    def __init__(self, solver: AcadosOcpSolver, simulator: AcadosSimSolver):
        self.solver = solver
        self.simulator = simulator
        self.dt = solver.acados_ocp.solver_options.tf / solver.N
        self.dim_x = self.solver.acados_ocp.model.x.rows()
        self.dim_u = self.solver.acados_ocp.model.u.rows()

        self.width = 1920
        self.height = 1080
        self.scale = 0.0005
        print(f"expected update frequency: {1 / self.dt}")
        self.running = False
        self.paused = False

        self.new_pose = None

        self.goal_pos = None
        self.goal_orientation = None

        # Initialize pygame
        pygame.init()
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("Robot Simulation")

        # Colors
        self.background_color = (255, 255, 255)  # White
        self.robot_color = (0, 128, 255)  # Blue
        self.goal_color = (255, 0, 0)  # Red
        self.path_color = (0, 255, 0)  # Traveled Path
        self.planned_color = (230, 230, 0)  #planned paths
        # Grid Specification
        self.grid_color = (200, 200, 200)  # Light Gray for grid lines
        self.grid_every = 0.1

        # Robot initial state [x, y, speed, orientation, rotation speed]
        self.current_x = np.array([100 * self.scale, 100 * self.scale, 0, 0, 0])  # Starting at center

        self.sim_path = []
        self.sim_control = []

        self.predicted_path = []

    def run(self):
        self.running = True
        clock = pygame.time.Clock()

        while self.running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.running = False
                    elif event.key == pygame.K_SPACE:
                        self.paused = not self.paused
                    elif event.key == pygame.K_g:
                        # plot results

                        plot_robot(
                            np.linspace(0, self.dt * len(self.sim_path), len(self.sim_path)), [main.a_max, None],
                            np.array(self.sim_control),
                            np.array(self.sim_path),
                            x_labels=self.solver.acados_ocp.model.x_labels,
                            u_labels=self.solver.acados_ocp.model.u_labels,
                            time_label=self.solver.acados_ocp.model.t_label
                        )

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    self.new_pose = pygame.mouse.get_pos()

                elif event.type == pygame.MOUSEBUTTONUP:
                    final_pos = pygame.mouse.get_pos()
                    goal_orientation = np.arctan2(final_pos[1] - self.new_pose[1], final_pos[0] - self.new_pose[0])
                    self.update_goal(self.new_pose, goal_orientation)
                    self.new_pose = None
                    self.sim_path.clear()
                    self.sim_control.clear()

            if not self.paused:
                if self.goal_pos is not None:
                    pred_x, pred_u = self.update_simulation(self.current_x, self.goal_pos, self.goal_orientation)
                    self.current_x = self.simulation_step(self.current_x, pred_u[0])

                    self.sim_path.append(self.current_x.copy())
                    self.sim_control.append(pred_u[0].copy())

                    self.predicted_path = pred_x
                self.draw(self.current_x)

            if self.new_pose is not None:  # Only draw the goal orientation if mouse is down
                self.draw_goal(self.new_pose, pygame.mouse.get_pos())

            pygame.display.flip()
            clock.tick(1 / self.dt)  # Limit to 60 frames per second

        pygame.quit()

    def update_simulation(self, current_x, goal_pose: Tuple[float, float], goal_orientation: float) -> Tuple[
        np.ndarray, np.ndarray]:
        """
        This function should NOT be modified but inherited from
        :param current_x:
        :param goal_pose:
        :param goal_orientation:
        :return:
        """

        N_horizon = self.solver.N

        # Fill the reference path
        ref_path = np.zeros((N_horizon, self.dim_x + self.dim_u))  # Adjust the size based on your state dimension
        ref_path[:, 0] = self.goal_pos[0]  # x position
        ref_path[:, 1] = self.goal_pos[1]  # y position
        ref_path[:, 3] = self.goal_orientation  # (theta) position
        ref_goal = np.array([self.goal_pos[0], self.goal_pos[1], 0, self.goal_orientation, 0])

        #Fill the initial guess
        guess_x = np.zeros((N_horizon + 1, self.dim_x))
        quess_u = np.zeros((N_horizon, self.dim_u))

        pred_x, pred_u = solve_problem(self.solver,
                                       current_x=current_x,
                                       guess_x=guess_x,
                                       guess_u=quess_u,
                                       ref_path=ref_path,
                                       ref_goal=ref_goal)

        return pred_x, pred_u

    def simulation_step(self, current_x, pred_u):
        new_x = self.simulator.simulate(current_x, pred_u)
        return new_x

    def update_goal(self, pos, orientation):
        self.goal_pos = np.array([pos[0] * self.scale, pos[1] * self.scale, orientation])
        self.goal_orientation = orientation
        # Set new goal in the solver here
        print(f"New goal position: {self.goal_pos} with orientation: {np.degrees(self.goal_orientation)} degrees")
        pass

    def draw(self, current_x):
        orientation = current_x[3]
        image_x = current_x / self.scale
        self.screen.fill(self.background_color)

        self.draw_grid()
        self.draw_path()
        # Draw robot as circle with orientation line
        pygame.draw.circle(self.screen, self.robot_color, (int(image_x[0]), int(image_x[1])), 20)
        end_pos = (int(image_x[0] + 40 * np.cos(orientation)),
                   int(image_x[1] + 40 * np.sin(orientation)))
        pygame.draw.line(self.screen, self.robot_color, (int(image_x[0]), int(image_x[1])), end_pos, 2)

        # Draw goal if exists
        if self.goal_pos is not None:
            image_goal = self.goal_pos / self.scale
            image_goal = (image_goal[0], image_goal[1])
            pygame.draw.circle(self.screen, self.goal_color, image_goal, 30, 1)
            if self.goal_orientation is not None:
                goal_end_pos = (image_goal[0] + 40 * np.cos(self.goal_orientation),
                                image_goal[1] + 40 * np.sin(self.goal_orientation))
                pygame.draw.line(self.screen, self.goal_color, image_goal, goal_end_pos, 1)

    def draw_goal(self, goal_pose, mouse_pose):
        self.screen.fill(self.background_color)
        self.draw(self.current_x)
        pygame.draw.line(self.screen, self.goal_color, goal_pose, mouse_pose, 1)

    def draw_grid(self):
        distance = self.grid_every / self.scale  # distance in pixels for each grid line
        for x in range(0, self.width, int(distance)):
            pygame.draw.line(self.screen, self.grid_color, (x, 0), (x, self.height))
        for y in range(0, self.height, int(distance)):
            pygame.draw.line(self.screen, self.grid_color, (0, y), (self.width, y))

    def draw_path(self):
        if len(self.sim_path) > 1:
            scaled_path = [(int(p[0] / self.scale), int(p[1] / self.scale)) for p in self.sim_path]
            pygame.draw.lines(self.screen, self.path_color, False, scaled_path, 2)
            for point in scaled_path:
                pygame.draw.circle(self.screen, self.path_color, point, 3)  # Draw marker

        if len(self.predicted_path) > 1:
            scaled_path = [(int(p[0] / self.scale), int(p[1] / self.scale)) for p in self.predicted_path]
            pygame.draw.lines(self.screen, self.planned_color, False, scaled_path, 2)
            for point in scaled_path:
                pygame.draw.circle(self.screen, self.planned_color, point, 3)


def solve_problem(solver, current_x, guess_x, guess_u, ref_path, ref_goal):
    N_horizon = solver.N
    # Fill in the data
    for stage, ref in enumerate(ref_path):
        solver.set(stage, "yref", ref)
    solver.set(N_horizon, "yref", ref_goal)

    for stage, guess in enumerate(guess_x):
        solver.set(stage, "x", guess)

    for stage, control in enumerate(guess_u):
        solver.set(stage, "u", control)

    # Solver the problem with current state
    solver.solve_for_x0(current_x, fail_on_nonzero_status=False, print_stats_on_failure=False)
    status = solver.get_status()

    if status not in [0, 2]:
        solver.print_statistics()
        raise Exception(
            f"acados acados_ocp_solver returned status {status} in closed loop instance with {current_x}"
        )

    # extract and store predicted trajectories

    dim_x = solver.acados_ocp.model.x.rows()
    dim_u = solver.acados_ocp.model.u.rows()

    predX = np.zeros((N_horizon + 1, dim_x))
    predU = np.zeros((N_horizon, dim_u))
    for j in range(N_horizon):
        predX[j, :] = solver.get(j, "x")
        predU[j, :] = solver.get(j, "u")
    predX[N_horizon, :] = solver.get(N_horizon, "x")

    return predX, predU
