from typing import Tuple
import numpy as np

from acados_template import AcadosOcpSolver, AcadosSimSolver

from robot_model import export_robot_model
from ocp_description import create_ocp_solver_description
from support.simulation import RobotSimulation
from support.simulation import solve_problem


def run():
    # Create model & problem
    robot_model = export_robot_model()
    ocp = create_ocp_solver_description(robot_model)
    # Use those to create magic:
    ocp_solver = AcadosOcpSolver(ocp)  #Solver to problem
    sim_solver = AcadosSimSolver(ocp)  #Simulation using our model

    # Run support with
    simulation = MyUpdateSimulation(solver=ocp_solver, simulator=sim_solver)
    simulation.run()


class MyUpdateSimulation(RobotSimulation):
    def __init__(self, solver: AcadosOcpSolver, simulator: AcadosSimSolver):
        super().__init__(solver, simulator)

    def update_simulation(self, current_x, goal_pose: Tuple[float, float], goal_orientation: float) -> Tuple[
        np.ndarray, np.ndarray]:
        N_horizon = self.solver.N

        # Fill the reference path
        ref_path = np.zeros((N_horizon, self.dim_x + self.dim_u))  # Adjust the size based on your state dimension
        ref_path[:, 0] = goal_pose[0]  # x position
        ref_path[:, 1] = goal_pose[1]  # y position
        ref_path[:, 3] = goal_orientation  # (theta) position
        ref_goal = np.array([goal_pose[0], goal_pose[1], 0, goal_orientation, 0])

        # Fill the initial guess
        guess_x = np.zeros((N_horizon + 1, self.dim_x))
        quess_u = np.zeros((N_horizon, self.dim_u))

        pred_x, pred_u = solve_problem(self.solver,
                                       current_x=current_x,
                                       guess_x=guess_x,
                                       guess_u=quess_u,
                                       ref_path=ref_path,
                                       ref_goal=ref_goal)

        return pred_x, pred_u


if __name__ == "__main__":
    run()
