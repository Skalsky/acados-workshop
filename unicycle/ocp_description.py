from acados_template import AcadosOcp, AcadosModel
from robot_model import export_robot_model
import numpy as np
import scipy.linalg

X0 = np.array([0.0, 0.0, 0.0, 0.0, 0.0])  # Intital state
a_max = 2  # Define the max acceleration allowed
T_horizon = 2.0  # Define the prediction horizon


def create_ocp_solver_description(robot_model: AcadosModel) -> AcadosOcp:
    N_horizon = 50  # Define the number of discretization steps

    # create ocp object to formulate the OCP
    ocp = AcadosOcp()

    #create model
    robot_model = export_robot_model()
    ocp.model = robot_model

    #dimensions of state & control
    nx = robot_model.x.rows()
    nu = robot_model.u.rows()

    # set dimensions
    ocp.dims.N = N_horizon

    # set cost
    Q_mat = 2 * np.diag([1e3, 1e3, 1e-4, 1e-1, 1e-4])  # [x,y,x_d,y_d,th,th_d]
    R_mat = 2 * 5 * np.diag([1e-1, 1e-3])

    ocp.cost.cost_type = "LINEAR_LS"
    ocp.cost.cost_type_e = "LINEAR_LS"

    ny = nx + nu
    ny_e = nx

    ocp.cost.W_e = Q_mat
    ocp.cost.W = scipy.linalg.block_diag(Q_mat, R_mat)

    ocp.cost.Vx = np.zeros((ny, nx))
    ocp.cost.Vx[:nx, :nx] = np.eye(nx)

    Vu = np.zeros((ny, nu))
    Vu[nx: nx + nu, 0:nu] = np.eye(nu)
    ocp.cost.Vu = Vu

    ocp.cost.Vx_e = np.eye(nx)

    ocp.cost.yref = np.zeros((ny,))
    ocp.cost.yref_e = np.zeros((ny_e,))

    # set constraints
    ocp.constraints.lbu = np.array([-a_max])
    ocp.constraints.ubu = np.array([+a_max])
    ocp.constraints.idxbu = np.array([0])

    ocp.constraints.x0 = X0

    # set options
    ocp.solver_options.qp_solver = "PARTIAL_CONDENSING_HPIPM"  # FULL_CONDENSING_QPOASES
    ocp.solver_options.hessian_approx = "GAUSS_NEWTON"
    ocp.solver_options.integrator_type = "IRK"
    ocp.solver_options.nlp_solver_type = "SQP"

    # set prediction horizon
    ocp.solver_options.tf = T_horizon

    return ocp


