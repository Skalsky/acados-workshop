
#setup init submodule(acados) and its submodules
git submodule update --recursive --init

#setup cmake build folder, build & install
cmake -S acados -B acados_build -DACADOS_WITH_QPOASES=ON
make install -j 8

pip install -e acados/interfaces/acados_template


#export variables so we can find binaries and sources
current_dir=$(pwd)
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$current_dir/acados/lib
export ACADOS_SOURCE_DIR=$current_dir/acados

